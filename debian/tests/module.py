import sys
import cssutils

sheet = cssutils.parseString('''
@variables { BG: #ab12cd }
html { background: var(BG) }
''')

css = sheet.cssText.decode()
expected = 'html {\n    background: #ab12cd\n    }'

print('got: %s\nexpected: %s' % (css, expected))
if css != expected:
    sys.exit(1)
